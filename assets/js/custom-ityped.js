(function ($) {
    'use strict';


    $(document).ready(function () {

        ityped.init(document.querySelector("#ityped"), {
            strings: ['spa.', 'hair salons.'],
            typeSpeed: 55,
            startDelay: 200,
            backSpeed: 20,
            backDelay: 1000,
            loop: true,
            cursorChar: "|",
            
        });

    });

})(jQuery);
